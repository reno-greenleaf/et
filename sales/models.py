from django.db import models
from personnel.models import Staff

class Drink(models.Model):
	""" Product of the coffeeshop. """
	name = models.CharField(max_length=256)
	price = models.FloatField()


class Order(models.Model):
	""" Data of a purchase. """
	seller = models.ForeignKey(Staff, on_delete=models.CASCADE)
	date = models.DateTimeField()
	total = models.FloatField()


class Sale(models.Model):
	""" Item in a list of purchased drinks of an order. """
	order = models.ForeignKey(Order, on_delete=models.CASCADE)
	drink = models.ForeignKey(Drink, on_delete=models.CASCADE)