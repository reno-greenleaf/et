from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from sales.models import Drink
from personnel.models import Staff

class TestAPI(TestCase):
	def setUp(self):
		manager = Staff.objects.get(username='lilydoe')
		self.client = APIClient()
		self.client.force_authenticate(user=manager)

	def test_add_drink(self):
		result = self.client.post('/drinks/', {'name': "Tea", 'price': '2.0'}, format='json')
		drinks = Drink.objects.filter(name="Tea").all()
		self.assertEqual(len(drinks), 1)

	def test_list_drinks(self):
		drink = Drink.objects.create(name="Tea", price=2)
		response = self.client.get('/drinks/', format='json')
		self.assertEqual(len(response.data), 1)

	def test_change(self):
		drink = Drink.objects.create(name="Late", price=2.3)
		self.client.put('/drink/%d' % drink.pk, {'name': "Latte", 'price': 2.4})
		changed_drink = Drink.objects.get(pk=drink.pk)
		self.assertEqual(changed_drink.price, 2.4)

	def test_access(self):
		seller = Staff.objects.get(username='marysue')
		client = APIClient()
		client.force_authenticate(user=seller)
		result = client.post('/drinks/', {'name': "Coffee", 'price': '1.5'}, format='json')
		self.assertEqual(result.status_code, status.HTTP_403_FORBIDDEN)

		drink = Drink.objects.create(name="Americano", price=2.3)
		result = client.put('/drink/%d' % drink.pk, {'name': "Latte", 'price': 2.4})
		self.assertEqual(result.status_code, status.HTTP_403_FORBIDDEN)