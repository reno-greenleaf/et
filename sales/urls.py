from django.urls import path
from sales.views import Drinks, DrinkChanges

urlpatterns = [
	path('drinks/', Drinks.as_view()),
	path('drink/<int:pk>', DrinkChanges.as_view())
]