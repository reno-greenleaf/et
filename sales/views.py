from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from sales.models import Drink
from sales.serializers import DrinkSerializer

class Drinks(APIView):
	""" Default actions with drinks. """
	permission_classes = [IsAuthenticated, DjangoModelPermissions]
	queryset = Drink.objects.none()

	def get(self, request, format=None):
		drinks = Drink.objects.all()
		sersilized = DrinkSerializer(drinks, many=True)
		return Response(sersilized.data)

	def post(self, request, format=None):
		serializer = DrinkSerializer(data=request.data)

		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=201)
		else:
			return Response(serializer.errors, status=400)


class DrinkChanges(APIView):
	""" Perform changes with a drink. """
	permission_classes = [IsAuthenticated, DjangoModelPermissions]
	queryset = Drink.objects.none()

	def get(self, request, pk, format=None):
		drink = Drink.objects.get(pk=pk)
		serialized = DrinkSerializer(drink)
		return Response(serialized.data)

	def put(self, request, pk, format=None):
		drink = Drink.objects.get(pk=pk)
		serializer = DrinkSerializer(drink, data=request.data)

		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk, format=None):
		drink = Drink.objects.get(pk=pk)
		drink.delete()
		return Response(status=status.HTTP_204_NO_CONTENT)