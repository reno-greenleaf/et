from django.urls import path
from personnel.views import Statistics

urlpatterns = [
	path('statistics/', Statistics.as_view())
]