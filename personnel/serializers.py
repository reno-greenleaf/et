from rest_framework.serializers import ModelSerializer, IntegerField, FloatField
from personnel.models import Staff

class StaffSerializer(ModelSerializer):
	orders = IntegerField()
	sold = FloatField()

	class Meta:
		model = Staff
		fields = ['first_name', 'last_name', 'position', 'orders', 'sold']