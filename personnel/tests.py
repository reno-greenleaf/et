from datetime import datetime as d
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient
from personnel.models import Staff
from sales.models import Order

class TestAPI(TestCase):
	def setUp(self):
		manager = Staff.objects.get(username='bobdillan')
		self.client = APIClient()
		self.client.force_authenticate(user=manager)

	def test_stats(self):
		john = Staff.objects.get(username='johndoe')
		mary = Staff.objects.get(username='marysue')

		Order.objects.create(seller=john, date=d(2019, 12, 31, 15, 20), total=2.1)
		Order.objects.create(seller=john, date=d(2020, 1, 3, 15, 30), total=3.0)
		Order.objects.create(seller=mary, date=d(2019, 12, 30, 12, 20), total=2.0)

		result = self.client.get('/statistics/')
		marys_stats = {'first_name': 'Mary', 'last_name': 'Sue', 'position': 'Seller', 'orders': 1, 'sold': 2.0}
		johns_stats = {'first_name': 'John', 'last_name': 'Doe', 'position': 'Seller', 'orders': 2, 'sold': 5.1}

		self.assertIn(marys_stats, result.data)
		self.assertIn(johns_stats, result.data)

	def test_access(self):
		seller = Staff.objects.get(username='johndoe')
		client = APIClient()
		client.force_authenticate(user=seller)
		result = client.get('/statistics/')
		self.assertEqual(result.status_code, status.HTTP_403_FORBIDDEN)