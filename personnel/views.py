from django.db.models import Count, Sum
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from personnel.models import Staff
from personnel.serializers import StaffSerializer
from personnel.permissions import DjangoModelPermissionsWithRead

class Statistics(APIView):
	""" Overview of productivity of a worker. """
	permission_classes = [IsAuthenticated, DjangoModelPermissionsWithRead]
	queryset = Staff.objects.none()

	def get(self, request, format=None):
		staff = Staff.objects.annotate(orders = Count('order'), sold=Sum('order__total')).all()
		serialized = StaffSerializer(staff, many=True)
		return Response(serialized.data)