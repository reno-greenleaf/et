from django.db import models
from django.contrib.auth.models import AbstractUser

class Staff(AbstractUser):
	""" Data about coffeeshop worker. """
	position = models.CharField(max_length=256)
	token = models.CharField(max_length=100)